# DecentralizedWebHosting

## About 

The project aimed at implementing a decentralized web hosting solution based on Interplanetary File System (IPFS) and Ethereum blockchain. IPFS manages all the nodes and file storage across the network. It is used to store data of the hosted web pages. Ethereum smart contracts are used to manage the IPFS network. Experimental results of decentralized apps show that, compared to the traditional web hosting model, the web application hosted across nodes in a decentralized manner ensures integrity, and availability.

## Experimental Setup
The project uses various technologies for different modules.
1. Frontend is created using ReactJS, version 17.0.2. Along with it various other libraries are used, information about which can be gained by looking the package.json file of frontend directory.(DWH/frontend/package.json)
2. Backend makes use of NodeJS, version ^16.15.0, npm version 8.5.5 is used. Information about all the other libraries used can be obtained from the package.json file.(DWH/server/package.json)
3. Truffle is used to setup a local ethereum blockchain environment. [React Truffle Box](https://trufflesuite.com/boxes/react/) is used to setup starting environment.
4. Javacript implmentation of ipfs is used in the project, version 0.14.2.

## Truffle and JS-IPFS installation
Truffle installation
```
npm install -g truffle
truffle unbox react
```

JS-IPFS installation
```
npm i -g ipfs
```

> **_NOTE:_**  js-ipfs only works with node and npm version greater than or equal to, 16.0.0 and 8.0.0 respectively.


## Setup environment to run the project

You will need to run the commands in four separate terminals
1. Initialize truffle
```
truffle develop
migrate
```
> **_NOTE:_** If running the project for the first time run "truffle config" before any truffle command.

2. Initialize js-ipfs
```
jsipfs daemon
```

3. Initialize backend
```
nodemon server
```
Run in directory DWH/server/ 

4. Initialize frontend
```
npm start
```
Run in directory DWH/frontend/


