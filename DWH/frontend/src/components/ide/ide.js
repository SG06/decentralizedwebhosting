import React, { useState, useEffect } from "react";
import { EditorState } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./ide.css";
import { convertToRaw } from "draft-js";
import axios from "axios";
import { Box, Button, Paper } from "@material-ui/core";
import { ContentState } from "draft-js";
import { Grid } from "@material-ui/core";
import { useParams } from "react-router-dom";
import { TextField } from "@mui/material";

const IDE = () => {
  const [editorState, setEditorState] = useState(() =>
    EditorState.createEmpty()
  );

  const [key, setKey] = useState('')

  const { path, filename } = useParams();

  const paperStyle = {
    padding: 20,
    height: "63vh",
    width: 300,
    margin: "0 auto",
  };

  useEffect(async () => {
    if (localStorage.text){
      setEditorState(
        EditorState.createWithContent(ContentState.createFromText(localStorage.text))
      );
    }
    else{
      axios
        .get(
          `https://ipfs.io/ipfs/${path}/${filename}`
        )
        .then((res) => {
          console.log(res);
          setEditorState(
            EditorState.createWithContent(ContentState.createFromText(res.data))
          );
        });
      }
  }, []);

  const handleUpload = () => {
    const blocks = convertToRaw(editorState.getCurrentContent()).blocks;
    const value = blocks
      .map((block) => (!block.text.trim() && "\n") || block.text)
      .join("\n");
    console.log(value);

    const clientID = window.localStorage.getItem("userID")
    const datatosend = {
      content: value,
      filepath: path+"/"+filename,
      cindex: JSON.parse(clientID).clientnumber,
      windex: JSON.parse(clientID).websites,
      key: key
    }

    axios
    .post("http://localhost:8000/reupload", datatosend)
    .then((res) => {
      console.log(res);
    })

    localStorage.removeItem("text")

    window.location.href = "/dashboard"
  };

  const save = () => {
    const blocks = convertToRaw(editorState.getCurrentContent()).blocks;
    const value = blocks
      .map((block) => (!block.text.trim() && "\n") || block.text)
      .join("\n");
    localStorage.setItem("text", value);
  }


  return (
    <Grid>
      <div>
        <header className="App-header">Edit your file here</header>
        <Editor
          editorState={editorState}
          onEditorStateChange={setEditorState}
          wrapperClassName="wrapper-class"
          editorClassName="editor-class"
          toolbarClassName="toolbar-class"
        />
        <TextField
            size="small"
            id="outlined-basic"
            label="Enter Key"
            variant="outlined"
            onChange={(e) => setKey(e.target.value)}
            style={{
              marginBottom: "10px",
            }}
        />
        <Grid container justify="center">
          <Button
            variant="contained"
            type="submit"
            color="primary"
            onClick={save}
          >
            Save
          </Button>
          <Button
            variant="contained"
            type="submit"
            color="primary"
            onClick={handleUpload}
          >
            Update
          </Button>
        </Grid>
      </div>
    </Grid>
  );
};
export default IDE;
 