import { useState } from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import "./style.css";
import axios from "axios";
import { TextField } from "@mui/material";
import { Box } from "@mui/material";
const FileUploader = () => {
  let clientID = window.localStorage.getItem("userID");
  let clientnumber = JSON.parse(clientID).clientnumber;
  let websites = JSON.parse(clientID).websites;

  const [open, setOpen] = useState(false);
  const [file, setFile] = useState(null);
  const [key, setKey] = useState('');

  const onSubmit = (e) => {
    setOpen(false);
    e.preventDefault();
    const formdata = new FormData();
    formdata.append("file", file);

    console.log(key)
    const data = {
      cindex: clientnumber,
      windex: websites,
      key: key
    }
    formdata.append("clientdata", JSON.stringify(data))
    
    axios
      .post("http://localhost:8000/upload", formdata)
      .then((res) => {
        console.log(res);
      })
      .catch((e) => {
        console.log("Error", e);
      });
  };

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClickCancel = () => {
    setOpen(false);
  };

  const handleClose = (e) => {
    console.log("Hellooo");
    console.log(e.target.files[0]);
    setFile(e.target.files[0]);
  };

  return (
    <div>
      <Box marginTop={10} textAlign="center">
        <Button
          variant="contained"
          type="submit"
          color="primary"
          onClick={handleClickOpen}
        >
          Upload Website
        </Button>
      </Box>

      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Upload website</DialogTitle>
        <DialogContent>
          <DialogContentText>
            To host your website ,upload your files in zip format
          </DialogContentText>
          <form method="post" action="#" id="#">
            <div className="files">
              <label>Upload Your File in Zip Format</label>
              <input
                onChange={handleClose}
                type="file"
                className="form-control"
                multiple=""
              />
            </div>
            <TextField
            size="small"
            fullWidth
            id="outlined-basic"
            label="Enter Key(Important. Required while editing)"
            variant="outlined"
            onChange={(e) => setKey(e.target.value)}
          />
          </form>
        </DialogContent>
        <DialogActions>
          <Button
            variant="contained"
            color="primary"
            onClick={handleClickCancel}
          >
            Cancel
          </Button>
          <Button variant="contained" color="primary" onClick={onSubmit}>
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default FileUploader;
