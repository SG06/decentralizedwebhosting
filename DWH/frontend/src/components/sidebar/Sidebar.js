import React from "react";
import "./sidebar.css";
import { SideBarData } from "./SideBarData";
function Sidebar() {
  return (
    <div className="Sidebar">
      <ul className="SidebarList">
        {SideBarData.map((val, key) => {
          return (
            <li className="row" onClick={()=>{
              window.location.href="/"
            }} key={key}>
              <div id="icon">{val.icon}</div> <div id="title">{val.title}</div>
            </li>
          );
        })}
      </ul>
    </div>
  );
}
export default Sidebar;
