import React from "react";
import HomeIcon from "@material-ui/icons/Home";
import MailIcon from "@mui/icons-material/Mail";
import AssessmentIcon from "@mui/icons-material/Assessment";
import WebIcon from "@mui/icons-material/Web";
import LogoutIcon from "@mui/icons-material/Logout";

export const SideBarData = [
  
  {
    title: "Logout",

    icon: <LogoutIcon />,
    link: "/logout",
  },
];
