import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button } from "@mui/material";
import { useParams } from "react-router-dom";


function createData(type, name, cid) {
  return { type, name, cid };
}


export default function EditorTable(props) {
  const rows = [];
  const array = props.directory;

  array.forEach(function (item, index) {
    rows.push(createData(item.type, item.name, item.cid));
  });

  const { cid } = useParams();

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Type</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>CID</TableCell>
            <TableCell>Edit</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.type}
              </TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.cid}</TableCell>

              <TableCell   >
                {row.type === "file" ? (
                  <Button  
                  onClick={()=>{
                    window.location.href=`/ide/${cid}/${row.name}`
                  }}
                  >Edit</Button>
                ) : (
                  <Button>View</Button>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
