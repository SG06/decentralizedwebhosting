import { AppBar } from "@material-ui/core";
import { Box } from "@material-ui/core";
import { Toolbar } from "@material-ui/core";
import { Typography } from "@material-ui/core";

export default function DenseAppBar() {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar variant="dense">
          <Typography
            align="center"
            variant="h6"
            color="inherit"
            component="div"
          >
            Decentralized Web Hosting Platform
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
