import * as React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button } from "@mui/material";
function createData(name, calories) {
  return { name, calories };
}

export default function BasicTable(props) {
  const rows = [];
  const array = props.directory;

  array.forEach(function (item, index) {
    rows.push(createData(item.hash, item.dirname));
  });

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Hash</TableCell>
            <TableCell>Name Website</TableCell>
            <TableCell>Link</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell>
                <Button
                  onClick={() =>{
                    console.log(row.calories)
                    window.localStorage.setItem("currHash",row.name)
                    window.location.href = `/editor/${row.name}`;
                  }}> 
                  {row.calories}
                  </Button>
                  </TableCell>
              <TableCell>
                <Button
                  onClick={() => {
                   console.log(`https://ipfs.io/ipfs/${row.name}`)
                   window.open(`https://ipfs.io/ipfs/${row.name}`, '_target');
                  }}
                >
                  Link
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
