import axios from "axios";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';

function readTextFile(file)
    {
   var rawFile = new XMLHttpRequest();
   rawFile.open("GET", file, false);
   rawFile.onreadystatechange = function ()
   {
       if(rawFile.readyState === 4)
       {
           if(rawFile.status === 200 || rawFile.status == 0)
           {
               var allText = rawFile.responseText;
               var textArea = document.getElementById("editor");
               console.log(textArea)
               if (localStorage.text){
                   textArea.innerHTML = localStorage.text;
               }
               else{
                   textArea.innerText = allText;
               }
               console.log(allText);
               ClassicEditor
                .create( document.querySelector( '#editor' ), {
                } )
                .then( editor => {
                    console.log(document.getElementById("editor").innerHTML);   
                    window.editor = editor;
                    } )
                    .catch( err => {
                        console.error( err.stack );
                    } );
                    }
                }
            }
            setTimeout(()=>rawFile.send(null),1000)
    }
var file = 'https://ipfs.io/ipfs/QmeXiQQTK1NsSXS6SVbndB28tR3tEXQWV6vKM9YpUZutCo/a.txt';
readTextFile(file)

function save(){
    var text = window.editor.getData();
    localStorage.setItem("text", text);
    console.log(localStorage.text);
}
document.getElementById("save").addEventListener('click', save);

function upload(){
    var text = window.editor.getData();
    console.log(text);
    axios.post('', {
        filename: '',
        content: text
    })
    .then(function(response){
        console.log(response);
        localStorage.removeItem("text");
    })
    .catch(function(response){
        console.log(response);
    });
}
var button = document.getElementById("upload").addEventListener('click', upload);

document.write(
    unescape("%3Cscript src='../src/components/editor/ckeditor.js' type='text/javascript'%3E%3C/script%3E")
  );
