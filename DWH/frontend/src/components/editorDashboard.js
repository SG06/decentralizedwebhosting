import Sidebar from "./sidebar/Sidebar";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Typography } from "@mui/material";
import EditorTable from "./editortable";
import axios from "axios";
import { useState, useEffect } from "react";

const EditorDashboard = () => {
  const [directory, setDirectory] = useState([]);
  useEffect(async () => {
    let currHash = window.localStorage.getItem("currHash")

    axios.get(`http://localhost:8000/dashboard/directory?currHash=${currHash}`).then((res) => {
      setDirectory(res.data.result);
    });
  }, []);
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container>
        <Grid height={1000} item xs={3}>
          <Sidebar />
        </Grid>
        <Grid marginTop={10} item xs={6}>
          {/* <Typography align="inherit">Client Number</Typography> */}

          <Typography
            textAlign="center"
            variant="h4"
            gutterBottom
            component="div"
          >
            Directory Structure of Website
          </Typography>
          <EditorTable directory={directory} />
          {/* <FileUploader></FileUploader> */}
        </Grid>
        <Grid item xs={3}>
          <Typography align="right"></Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default EditorDashboard;
