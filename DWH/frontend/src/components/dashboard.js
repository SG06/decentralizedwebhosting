import FileUploader from "./FileUploader/index";
import Sidebar from "./sidebar/Sidebar";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { Typography } from "@mui/material";
import BasicTable from "./table";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";

const Dashboard = () => {
  let clientID = window.localStorage.getItem("userID");
  let clientnumber = JSON.parse(clientID).clientnumber;
  const [hash, setHash] = useState([]);

  useEffect(() => {
    axios
      .get(`http://localhost:8000/dashboard/?clientnumber=${clientnumber}`)
      .then((res) => {
        setHash(res.data.result);
      });
  },[]);

  return (
    <Box sx={{ flexGrow: 1 }}>
      <Grid container>
        <Grid height={1000} item xs={3}>
          <Sidebar />
        </Grid>
        <Grid marginTop={10} item xs={6}>
          <Typography variant="h5" align="inherit">
            Client Number - {clientnumber+1}
          </Typography>

          <Typography
            marginTop={10}
            textAlign="center"
            variant="h4"
            gutterBottom
            component="div"
          >
            Hosted websites
          </Typography>
          <BasicTable directory={hash} />
          <FileUploader></FileUploader>
        </Grid>
        <Grid item xs={3}>
          <Typography align="right"></Typography>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Dashboard;
