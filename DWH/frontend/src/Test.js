import Web3 from "web3";
import SimpleStorage from '../../truffle/client/src/contracts/SimpleStorage.json';


function Test() {
    const web3 = new Web3('http://localhost:8545');

    const id = web3.eth.net.getID();
    const deployedNetwork = SimpleStorage.networks[id];
    const instance = new web3.eth.Contract(
        SimpleStorage.abi,
        deployedNetwork.address
    );
    const result = instance.methods.get().call
    console.log(result);
}

export default Test;