import "./App.css";
import SignInOutContainer from "./components/Authentication/signInOut";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import DenseAppBar from "./components/navbar";
import Dashboard from "./components/dashboard";
import EditorDashboard from "./components/editorDashboard";
import IDE from "./components/ide/ide";
function App() {
  return (
    <Router>
      <DenseAppBar />

      <Routes>
        <Route path="/" element={<SignInOutContainer />} />
        <Route path="/ide/:path/:filename" element={<IDE />} />

        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/editor/:cid" element={<EditorDashboard />} />
      </Routes>
    </Router>
  );
}

export default App;
