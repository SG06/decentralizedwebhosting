var SimpleStorage = artifacts.require("../contracts/SimpleStorage.sol");
var Register = artifacts.require("../contracts/Registration.sol");

module.exports = function(deployer) {
  deployer.deploy(SimpleStorage);
  deployer.deploy(Register);
};
