// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

contract SimpleStorage {
  uint storedData = 2;

  function set(uint x) external {
    storedData = x;
  }

  function get() public view returns (uint) {
    return storedData;
  }
}
