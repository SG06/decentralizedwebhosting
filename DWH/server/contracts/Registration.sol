// SPDX-License-Identifier: MIT
pragma solidity ^0.8.11;

contract Registration {

    struct client {
        string name;
        string email;
        string password;
        uint256 phonenumber;
        uint websites;
    }


    struct ipfs_details {
        string pubhash;
        string originalhash;
        uint deploydate;
        string dirname;
        string key;
    }


    struct website_details {
        mapping(uint => ipfs_details) hash_info;
        bool is_deleted;
    }


    mapping(uint => website_details) client_hosting_info;


    client[] clients;


    uint[] emptyseats;


    function enroll_user(string memory _clntname, string memory _email, string memory _passwd, uint256 _phone) external {
        if(emptyseats.length != 0) {
            clients[emptyseats.length - 1] = client(_clntname, _email, _passwd, _phone, 0);
            emptyseats.pop();
        } else {
            clients.push(client(_clntname, _email, _passwd, _phone, 0));
        }
    }


    function get_users_size() external view returns (uint) {
        return clients.length;
    }


    function check_client_login(string memory _email) external view returns (int, int) {
        for(uint i=0; i<clients.length; i++) {
            if(keccak256(abi.encodePacked(clients[i].email)) == keccak256(abi.encodePacked(_email))) {
                return (int(i), int(clients[i].websites));
            }
        }
        return (-1, -1);
    }

    
    function check_client_register(string memory _email, string memory _name) external view returns (int, int) {
        for(uint i=0; i<clients.length; i++) {
            if((keccak256(abi.encodePacked(clients[i].name)) == keccak256(abi.encodePacked(_name))) || 
            (keccak256(abi.encodePacked(clients[i].email)) == keccak256(abi.encodePacked(_email)))) {
                return (int(i), int(clients[i].websites));
            }
        }
        return (-1, -1);
    }


    function get_client(uint _index) external view returns (client memory) {
        return clients[_index];
    }

    function get_clientname(uint _index) external view returns (string memory) {
        return clients[_index].name;
    }


    function get_hash(uint _index) external view returns (string memory) {
        return clients[_index].password;
    }
    

    function get_websites(uint _index) external view returns (uint) {
        return clients[_index].websites;
    }

    function store_newhash(uint _clientindex, string calldata _pubhash, string calldata _orghash, string calldata dirname, string calldata key) public {
        client_hosting_info[_clientindex].hash_info[clients[_clientindex].websites] = ipfs_details(_pubhash, _orghash, block.timestamp, dirname, key);
        clients[_clientindex].websites++;
    }

    function update_hash(uint _clientindex, uint _websiteindex, string calldata _pubhash, string calldata _orghash, string calldata dirname, string calldata key) public {
        client_hosting_info[_clientindex].hash_info[_websiteindex] = ipfs_details(_pubhash, _orghash, block.timestamp, dirname, key);
    }

    function get_dirname(uint _clientindex, uint _website_num) external view returns (string memory) {
        return client_hosting_info[_clientindex].hash_info[_website_num].dirname;
    }

    function get_pubhash(uint _clientindex, uint _website_num) external view returns (string memory) {
        return client_hosting_info[_clientindex].hash_info[_website_num].pubhash;
    }

    function get_orghash(uint _clientindex, uint _website_num) external view returns (string memory) {
        return client_hosting_info[_clientindex].hash_info[_website_num].originalhash;
    }

    function get_website_password(uint _clientindex, uint _website_num) external view returns (string memory){
        return client_hosting_info[_clientindex].hash_info[_website_num].key;
    }

    function remove_client(uint _index) external {
        emptyseats.push(_index);
        delete clients[_index];
    }
}

// contract HostWeb {
    
//     struct hosting_details {
//         string pubhash;
//         string originalhash;
//         uint deploydate;
//     }

//     mapping(uint => mapping(uint => hosting_details)) hostinfo;

//     uint customerCode;
//     Register getcode;

//     constructor() {
//         getcode = new Register();
//     }

//     function host(bytes32 secretcode, string calldata _pubhash, string calldata _orghash) public {
//         getcode result;
//         result = getcode.get_number_of_wesites(secretcode);
//         hostinfo[result[0]][result[1]] = hosting_details(_pubhash, _orghash, block.timestamp);
//     }

// }