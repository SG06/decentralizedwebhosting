const upload = require("./fileuploader");
const ipfs = require('./ipfs');
const contracts = require('./web3');
const bcrypt = require('bcrypt');
const decompress = require('decompress');
const fs = require('fs');
const uint8ArrayToString = require('uint8arrays/to-string');
const { get } = require("http");


const saltrounds = 10;

const extract_data = (files) => {
  const dirdata = [];

  files.forEach((file) => {
    if (file.type != "directory") {
      dirdata.push({
        path: file.path,
        content: file.data.toString(),
      });
    }
  });
  return dirdata;
};

const host = async (fname, clientindex, website_index, keypass) => {
  const currentfilename = fname;

  const milliseconds = new Date().valueOf();

  targetfile = "./compressed/" + currentfilename;
  destinationfile = "./decompressed/" + milliseconds;

  const result = decompress(targetfile, destinationfile)
    .then(async (files) => {
      console.log(`${files.length} files successfully decompressed!`);

      var uploadname = "";
      if (files.length == 1) {
        uploadname = fs.readdirSync("./decompressed/" + milliseconds)[0];
      } else {
        uploadname = currentfilename.split(".")[0];
      }

      const data_to_upload = extract_data(files);

      console.log(uploadname)
      const realhash = await ipfs.iupload(data_to_upload, uploadname);
      console.log("Realhash", realhash);

      const contract = await contracts.get_instance();

      const keyname = await contract.methods.get_clientname(clientindex).call();

      const key = await ipfs.newkey(keyname);
      const pubhash = await ipfs.publish(key.name, realhash.currhash);

      // This is temporary only for testing, will be removed in future
      const removedkey = await ipfs.removekey(key.name);

      const acc = await contracts.account;
      const receipt = await contract.methods
        .store_newhash(clientindex, pubhash.name, realhash.currhash, uploadname, keypass)
        .send({
          from: acc[0],
          gas: 3000000,
        });

      console.log(receipt)

      fs.rmSync("./decompressed/" + milliseconds, { recursive: true });
      fs.rmSync("./compressed/" + currentfilename, { recursive: true });

      return { dirname: uploadname, cid: realhash.currhash };
    })
    .catch((err) => {
      return "error";
    });

  return result;
};

const remove = async (clientnumber) => {
  const contract = await contracts.get_instance();
  const receipt = await contract.methods.remove_client(clientnumber).send({
    from: acc[1],
    gas: 3000000,
  });

  if (receipt) {
    return "Client profile removed";
  } else {
    return "Server Error";
  }
};

const register = async (username, email, phone, password) => {
  const hashedpasswd = await new Promise((resolve, reject) => {
    bcrypt.hash(password, saltrounds, function (err, hash) {
      if (err) reject(err);
      resolve(hash);
    });
  });

  const acc = await contracts.account;

  const contract = await contracts.get_instance();

  const already_registered = await contract.methods
    .check_client_register(email, username)
    .call();

  if (already_registered[0] == "-1") {
    const receipt = await contract.methods
      .enroll_user(username, email, hashedpasswd, parseInt(phone))
      .send({
        from: acc[3],
        gas: 300000,
      });
    console.log(receipt);

    const count = await contract.methods.get_users_size().call();
    return { registered: 0, result: { 0: count - 1, 1: 0 } };
  } else {
    return { registered: 1, result: already_registered };
  }
};

const login = async (email, password) => {
  const contract = await contracts.get_instance();
  const register_info = await contract.methods.check_client_login(email).call();
  if (register_info[0] != "-1") {
    const client_passwd = await contract.methods
      .get_hash(register_info[0])
      .call();
    const result = await new Promise((resolve, reject) => {
      bcrypt.compare(password, client_passwd, (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    });

    if (result == true) {
      return register_info;
    }
  }
  return { 0: "-1", 1: "-1" };
};

const dashboard = async (clientnumber) => {
  const contract = await contracts.get_instance();

  var hashes = [];
  const no_of_websites = await contract.methods
    .get_websites(clientnumber)
    .call();

  for (var i = 0; i < no_of_websites; i++) {
    const hash = await contract.methods.get_orghash(clientnumber, i).call();
    const dirname = await contract.methods.get_dirname(clientnumber, i).call();

    hashes.push({
      hash: hash,
      dirname: dirname
    })
  }


  if (hashes.length == no_of_websites) {
    return hashes;
  } else {
    return "mismatcherror";
  }
};

const get_subdirectories = async (cid) => {
    files = await ipfs.ilist(cid);
    return files;
}

const parsedata = (data, filepath, newdata, dirname) => {
    var new_data_toupload = [];
    for (index in data) {
        if(data[index].type == "file") {
            if(data[index].path == filepath) {
                data[index].content = newdata
            }
            new_data_toupload.push({
                path: dirname + "/" + data[index].path.split("/").splice(1).join("/"),
                content: data[index].content
            })
        }
    }
    return new_data_toupload;
}

const reupload = async (newcontent, filepath, clientindex, website_index, key) => {

    const contract = await contracts.get_instance();

    const pwd = await contract.methods.get_website_password(clientindex, website_index).call();
    console.log(pwd)
    if (pwd != key){
      return -1
    }

    const response = await ipfs.idownload(filepath.split("/")[0]);

    const dirname = await contract.methods.get_dirname(clientindex, website_index).call();
    const data_to_upload = parsedata(response, filepath, newcontent, dirname);

    const realhash = await ipfs.iupload(data_to_upload, dirname);
    const keyname = await contract.methods.get_clientname(clientindex).call();
    
    const pubhash = await ipfs.publish(keyname, realhash.currhash);
        
    const acc = await contracts.account;
    const receipt = await contract.methods.update_hash(clientindex, website_index, pubhash.name, realhash.currhash, dirname, key).send({
        from: acc[0],
        gas: 3000000
    });

    console.log(receipt);

    return realhash.currhash;
}


module.exports = {
    host: host,
    register: register,
    login: login,
    dashboard: dashboard,
    remove: remove,
    directoryview: get_subdirectories,
    reupload: reupload,
}
