const Web3 = require("web3");
const Registration = require('../client/abi/Registration.json')


const getWeb3 = () => 
    new Promise((resolve, reject) => {
        window.addEventListener("load", async () => {
            if(window.ethereum) {
                const web3 = new Web3(window.ethereum);
                try {
                    await window.ethereum.enable();
                    resolve(web3);
                }
                catch(err) {
                    reject(err);
                }
            }
            else if(window.web3){
                const web3 = window.web3;
                console.log("Injected web3 detected!");
                resolve(web3);
            }
            else {
                const provider = new Web3.provider.HttpProvider("http://localhost:8545");
                const web3 = new Web3(provider);
                console.log("No web3 instance injected, using local web3");
                resolve(web3);
            }
        });
    }); 


// const web3 = getWeb3();
const web3 = new Web3('http://localhost:8545');


// Use web3 to get the user's accounts.
const accounts = web3.eth.getAccounts();


const init_contract = async () => {

    // Get the contract instance.
    const networkId = await web3.eth.net.getId();
    const deployedNetwork = Registration.networks[networkId];
    const instance = new web3.eth.Contract(
        Registration.abi,
        deployedNetwork && deployedNetwork.address,
    );

    return instance;
}


module.exports = {
    get_instance: init_contract,
    account: accounts
};