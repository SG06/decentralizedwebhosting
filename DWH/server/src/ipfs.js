const { create, CID } = require('ipfs-http-client');
const all = require('it-all')
const { extract } = require('it-tar')
const { pipe } = require('it-pipe')
const toBuffer = require('it-to-buffer')
const map = require('it-map')
const uint8ArrayToString = require('uint8arrays/to-string')


function ipfsClient() {
    // const ipfs = create({
    //     host: "ipfs.infura.io",
    //     port: 5001,
    //     protocol: "https"
    // });
    const ipfs = create({
        host: "localhost",
        port: 5002,
        protocol: "http"
    });
    return ipfs;
}


// function extract_data(dirname, nodes) {
        
//     try {
//         const files = fs.readdirSync(dirname);
//         if(files.length) {
//             for (const name of files) {
//                 extract_data(dirname+"/"+name, nodes);
//             }
//         }            
//     }
//     catch(err) {
//         const data = fs.readFileSync(dirname);
//         nodes.push({
//             path: dirname,
//             content: data.toString()
//         });
//     }

//     return;
// }


const ipfsupload = async (data, dirname) => {
    const ipfs = ipfsClient();
    
    // const dir_data = [];
    var output = "";

    // extract_data(dirname, dir_data);

    for await (const result of ipfs.addAll(data)) {
        // output = result;
        dir_hash = result.path==dirname ? result.cid.toString() : '';
        console.log(result)
    }

    // const ipns_hash = await ipfs.name.publish(dir_hash)
    return {
        currhash: dir_hash,
        // pubhash: ipns_hash
    };
}


const ipfsdownload = async (cid) => {
    const ipfs = ipfsClient();

    var data = [];
    async function * tarballed (source) {
        yield * pipe(
            source,
            extract(),
            async function * (source) {
                for await (const entry of source) {
                    yield {
                        ...entry,
                        body: await toBuffer(map(entry.body, (buf) => buf.slice()))
                        // body: await toBuffer(entry.body)
                    }
                }
            }
        )
    }
    
    async function collect (source) {
        return all(source)
    }

    const output = await pipe(
        ipfs.get(cid),
        tarballed,
        collect
    )
    
    output.map((file) => {
        data.push({
            path: file.header.name,
            content: uint8ArrayToString.toString(file.body),
            type: file.header.type
        })
    });

    return data;
}

const ipfslist = async (cid) => {
    const ipfs = ipfsClient();
    
    var subdir = []
    for await (const file of ipfs.ls(cid)) {
        subdir.push({
            name: file.name,
            cid: file.cid.toString(),
            type: file.type
        })
    }
    return subdir;
}

const get_newkey = async (name) => {
    const ipfs = ipfsClient();
    const newkey = await ipfs.key.gen(name);
    return newkey;
}

const removekey = async (name) => {
    const ipfs = ipfsClient();
    const key = ipfs.key.rm(name);
    return key;
}

const generate_public_hash = async (key, real_cid) => {
    const ipfs = ipfsClient();
    try {
        const pubhash = await ipfs.name.publish(real_cid, { key: key });
        return pubhash;
    } catch(error) {
        return error;
    }
}


module.exports = {
    iupload: ipfsupload,
    idownload: ipfsdownload,
    newkey: get_newkey,
    publish: generate_public_hash,
    ilist: ipfslist,
    removekey: removekey,
}