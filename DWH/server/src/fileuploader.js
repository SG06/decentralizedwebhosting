const  multer = require("multer");

// var fname = '';

const storage = multer.diskStorage({
    destination:  (req,file,cb)=>{
        cb(null,'compressed');
    },
      filename: (req,file,cb)=>{
        cb(null, file.originalname);
        // fname = file.originalname;
    }
});


// const upload = multer({storage}).single('file');


module.exports = {
    store: storage,
}