const express = require("express");
const cors = require("cors");
const main = require("./src/host");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const multer = require("multer");
const multerstorage = require("./src/fileuploader");
const fs = require("fs");

const app = express();
app.use(
  cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST"],
    credentials: true,
  })
);

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: "true" }));
app.use(bodyParser.json());

const upload = multer({ storage: multerstorage.store });

if (!fs.existsSync("./compressed")) {
  fs.mkdirSync("./compressed");
}

if (!fs.existsSync("./decompressed")) {
  fs.mkdirSync("./decompressed");
}

app.post("/upload", upload.single("file"), async (req, res) => {

  const data = JSON.parse(req.body.clientdata)
  const response = await main.host(
    req.file.originalname,
    data.cindex,
    data.windex,
    data.key
  );

  if (response == "error") {
    return res.status(200).json({
      status: 0,
      result: "Internal Server Error",
    });
  } else {
    return res.status(200).json({
      status: 1,
      result: response,
    });
  }
});

app.post("/dashboard/remove", async (req, res) => {
  const response = await main.remove(req.body.cindex);
  if (response == "success") {
    return res.status(200).json({
      status: 1,
      result: response,
    });
  } else {
    return res.status(500).json({
      status: 0,
      result: response,
    });
  }
});

app.post("/register", async (req, res) => {
  const client_info = await main.register(
    req.body.username,
    req.body.email,
    req.body.phone,
    req.body.password
  );
  console.log(client_info);
  if (client_info["registered"] == 1) {
    return res.status(400).json({
      status: 0,
      result: "User already registered",
    });
  } else {
    return res.status(200).json({
      status: 1,
      result: {
        clientnumber: client_info["result"][0],
        websites: client_info["result"][1],
      },
    });
  }
});

app.post("/login", async (req, res) => {
  const client_info = await main.login(req.body.email, req.body.password);
  if (client_info[0] == "-1") {
    return res.status(500).json({
      status: 0,
      result: "User not registered",
    });
  } else {
    return res.status(200).json({
      status: 1,
      result: {
        clientnumber: client_info[0],
        websites: client_info[1],
      },
    });
  }
});

app.get("/dashboard", async (req, res) => {
  const response = await main.dashboard(req.query.clientnumber);

  if (response == "error") {
    res.status(200).json({
      status: 0,
      result: "User not registered",
    });
  } else {
    res.status(200).json({
      status: 1,
      result: response,
    });
  }
});

app.get("/dashboard/directory", async (req, res) => {
 const currHash = req.query.currHash;
  const response = await main.directoryview(
    currHash
  );
  res.status(200).json({
    status: 1,
    result: response,
  });
});

app.post("/reupload", async(req, res) => {
    const response = await main.reupload(
        req.body.content,
        req.body.filepath,
        req.body.cindex,
        req.body.windex,
        req.body.key,
    );
    if (response == -1){
      res.status(200).json({
        status: 0,
        result: "Password Mismatch!"
      })
    }
    else {
      res.status(200).json({
          status: 1,
          result: response,
      });
    }
})

app.listen(8000, () => {
  console.log("App is running on port 8000");
});
